# lora-iot19-firmware

Ce repository contient les fichiers de code et la documentation du groupe firmware du projet de classe IOT-2019 de la HEIG-VD.

Le dossier **code** contient les fichiers de code. Le dossier **doc** contient les fichiers relatif à la documentation.