

// toggle oubton A10
setWatch(function() { 
  E12.toggle();
}, A10, { repeat:true, edge:'both', debounce:10});




// setup LoRa sur Serial 3
var RN2483 = require("RN2483");
Serial3.setup(57600/25*8, { tx:D8, rx:D9 });
var lora = new RN2483(Serial3, {reset:E13});


// Status du module
lora.getStatus(function(x){console.log(x);});


// Receiver
setInterval(function(){
  lora.radioRX(2000 , function(data) { //50000ms = 2 seconds
    if (data === undefined) {
      console.log("No data received");
    } else {
      console.log("Got data: "+JSON.stringify(data));
    }
  });
}, 1000);


save();