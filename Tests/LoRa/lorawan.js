//========================================================================================//
//====================                  Périphériques                 ====================//
//========================================================================================//


// Configuration et initialisation du port UART 3 :
Serial3.setup(57600/25*8, { tx:D8, rx:D9 });

// Congifuration et initialisation du bus I2C :
var i2c = new I2C();
i2c.setup({sda:C9,scl:A8});



//========================================================================================//
//====================                  STATE_MACHINE                 ====================//
//========================================================================================//


// Machine d'état du système. Au démarrage, elle est lancée, et tente de se
// connecter au réseau LoRa.


// Etats de la machine :
var LORA_STATE_INIT        = "INIT";
var LORA_STATE_SET_APPEUI  = "SETTING APPEUI";
var LORA_STATE_SET_APPKEY  = "SETTING APPKEY";
var LORA_STATE_JOINING     = "JOINING LORAWAN";
var LORA_STATE_CONNECTED   = "CONNECTED";
var LORA_STATE_ERROR       = "ERROR";



var serialData = "";
var loraState  = LORA_STATE_INIT;        // Variable d'état

var payload = "";
var error = 0;


// L'appEUI et l'appKEY en dure (car tag NFC ne fonctionne pas)
var appeui = "70B3D57ED0018DAC";
var appkey = "3AB62F727732E9B6BB60F0572D1E846B";


// Fonction de reception du message de l'uart sur le port
// Serial 3 (associé au module LoRa pour notre montage)
Serial3.on('data', function (data) { 
  serialData += data;
  if(serialData.indexOf("\r\n") != -1){
    //console.log(serialData);
    
    switch(loraState){
        
      //--------------------------------------------------------------//
      //-----------------        STATE_MACHINE       -----------------//
      //--------------------------------------------------------------//
        
      // Etat initiale (au démarage)
      case LORA_STATE_INIT :
        if(serialData.indexOf("RN2483 ") !== -1){
          console.log("[RN2483] Started !");
          // Basculation dans l'état "LORA_STATE_SET_APPEUI"
          loraState = LORA_STATE_SET_APPEUI;
          RN2483macSet(Serial3,CMD_APPEUI,appeui);
        }
        break;
    
      // Etat de configuration de l'appEUI
      case LORA_STATE_SET_APPEUI :
        if(serialData.indexOf(RN2483_MAC_RESP_OK) !== -1){
          console.log("[RN2483] Setting Appeui : " + serialData);
          // Basculation dans l'état "LORA_STATE_SET_APPKEY"
          loraState = LORA_STATE_SET_APPKEY;
          RN2483macSet(Serial3,CMD_APPKEY,appkey);
        }else{
          error = 1;
        }
        break;
        
      // Etat de configuration de l'appKEY
      case LORA_STATE_SET_APPKEY :
        if(serialData.indexOf(RN2483_MAC_RESP_OK) !== -1){
          console.log("[RN2483] Setting Appkey : " + serialData);
          // Basculation dans l'état "LORA_STATE_JOINING"
          loraState = LORA_STATE_JOINING;
          RN2483macJoin(Serial3,MODE_OTAA);
        }else{
          error = 1;
        }
        break;
        
        
      // Etat pour la connexion au réseau LoRa
      case LORA_STATE_JOINING :
        if(serialData.indexOf(RN2483_MAC_RESP_OK) !== -1){
          
          console.log("[RN2483] Joining network : join request sent... ");
          
        }else if(serialData.indexOf(RN2483_MAC_RESP_DENIED) !== -1){
          
          console.log("[RN2483] Joining network : access denied !");
          setInterval(function() {
            E15.toggle();
          }, 1000);
          
        }else if(serialData.indexOf(RN2483_MAC_RESP_ACCEPTED) !== -1){
          
          E15.toggle();
          setTimeout(function(){
            E15.toggle();
          },1000);
          
          console.log("[RN2483] Joining network : connected !");
          // Basculation dans l'état "LORA_STATE_CONNECTED"
          loraState = LORA_STATE_CONNECTED;
          
          // Démarre la récupération des données des capteurs :
          // Different action dépendant du capteur
          switch(sensor){
            case ENVIRONMENT_SENSOR :
            case INFRA_RED_SENSOR :
              setInterval(function() {
                payload = getSensorData();
                console.log("[RN2483] sending payload : " + payload);
                RN2483macTx(Serial3,TX_CNF,1,strToHex(payload));
              }, sending_interval);
              break;
              
            case TAMPER_SENSOR :
              setWatch(function() {
                console.log("[RN2483] Tamper Sensor Activated !");
                console.log("[RN2483] sending payload : " + TAMPER_ACTIVE_PAYLOAD);
                RN2483macTx(Serial3,TX_CNF,1,strToHex(TAMPER_ACTIVE_PAYLOAD));
              }, TAMPER_PIN_INT, {repeat:true, edge:'rising', debounce:10});
              break;
          }
          
          
        }else{
          error = 1;
        }
        break;
        
       
      // Etat losque la machine est connectée au réseau LoRa
      case LORA_STATE_CONNECTED :
        
        if(serialData.indexOf(RN2483_MAC_RESP_MAC_TX_OK) !== -1){
          
          console.log("[RN2483] Payload sent.");
          
        }else if(serialData.indexOf(RN2483_MAC_RESP_MAC_RX) !== -1){
          
          console.log("[RN2483] " + serialData);
          
          var start = serialData.indexOf(" ", 7) + 1;
          var data_rec  = hexToStr(serialData.substring(start));
          
          //sending_interval = parseInt(data_rec);
          console.log("[RN2483] Data : " + data_rec);
          //test = "5678";
          //save();
          
          
        }else if(serialData.indexOf(RN2483_MAC_RESP_NO_FREE_CHANNEL) !== -1){
          
          console.log("[RN2483] no free channel. Trying again...");
          
        }else if(serialData.indexOf(RN2483_MAC_RESP_OK) !== -1){
          
        }else if(serialData.indexOf(RN2483_MAC_RESP_BUSY) !== -1){
          console.log("[RN2483] Busy !");
          E15.toggle();
          setTimeout(function(){
            E15.toggle();
          },200);
        }else{
          error = 1;
        }
        break;
       
      default :
        break;
    }
    
    // En cas d'erreur, indique l'état dans lequel se trouvait
    // la machine ainsi que le message qui a généré l'erreur.
    if(error == 1){
      console.log("[RN2483] Error !");
      console.log("[RN2483] State :  " + loraState);
      console.log("[RN2483] " + serialData);
      loraState = LORA_STATE_ERROR;
      error = 0;
      setInterval(function() {
        E12.toggle();
      }, 500);
    }
    
    serialData = "";
  }
});


//========================================================================================//
//====================                      UTILS                     ====================//
//========================================================================================//


// Fonction convertissant un string en un sting correspondant à ses valeurs
// hexadécimale (pour le payload LoRa).
function strToHex(str){
    var char, i;

    var result = "";
    for (i=0; i<str.length; i++) {
        char = parseInt(str.charCodeAt(i));
        result += char.toString(16);
    }

    return result;
}


// Fonction qui convertit une string en hexa dans une string en une string en
// la string d'origine
function hexToStr(hex_str){
    var char, i, digit;
  
    var result = "";
    for (i=0; i<hex_str.length; i+=2) {
      char = "0x" + hex_str.charAt(i) + hex_str.charAt(i+1);
      result += String.fromCharCode(char);
    }
  
    return result;
}



//========================================================================================//
//====================                  INIT FUNCTION                 ====================//
//========================================================================================//


// Fonction appelée au boot de la carte
function onInit (){
  console.log("%%%%%%%%%%%%%%%%    IOT MODULE INIT    %%%%%%%%%%%%%%%%");
  
  // Pulse pour reset
  digitalPulse(E13, '0', 500);
  digitalWrite(E13, '1');
  
  // Allume les deux led, pour indiquer à l'utilisateur que la carte est en cours de démarage
  E12.toggle();
  E15.toggle();
  
  // Attend 1 seconde après le démarage pour s'assurer que le carte est dans
  // un état stable
  setTimeout(function(){
    
    // Configure l'état initiale de la machie d'état
    loraState = LORA_STATE_INIT;
    
    // En fonction du capteur, initialise une variable associée
    switch(sensor){
      case ENVIRONMENT_SENSOR :
        bme = require("BME680").connectI2C(i2c, {addr:0x77});
        break;
        
      case INFRA_RED_SENSOR :
        init_ir(i2c);
        break;
        
      case TAMPER_SENSOR :
        break;
        
    }
    
    
    // Eteint les deux led, pour indiquer que le boot est terminé
    E12.toggle();
    E15.toggle();
    
    console.log("%%%%%%%%%%%%%%%%       INIT DONE       %%%%%%%%%%%%%%%%");
    digitalWrite(E13, '1');
  },1000);
}



//========================================================================================//
//====================                      SENSORS                   ====================//
//========================================================================================//




var ENVIRONMENT_SENSOR = "ENV_SENS";
var INFRA_RED_SENSOR   = "IFR_SENS";
var TAMPER_SENSOR      = "TAM_SENS";



var sensor = ENVIRONMENT_SENSOR;



// Interval entre chaque mesure (en ms)
var sending_interval = 120000; // 30000 ms


// Fonction permettant de récupérer les donnnées
// du capteur actuellement configuré (dans la variable
// sensor). Dans le cas du TAMPER_SENSOR, qui agit par 
// interruption  la récupération des données se fait 
// uniquement dans la machine d'état.
function getSensorData(){
  
  var data;
  
  switch(sensor){
    case ENVIRONMENT_SENSOR :
      data = getEnvironmentData(bme);
      break;
      
    case INFRA_RED_SENSOR :
      data = getInfraRedData();
      break;
    
    default :
      console.log("[getSensorData fun] unknown sensor : " + sensor);
      return "error";
  }
  
  return data;
}


// Les variable suivante décrivent le format des payload des données
// envoyée.
// ils se décomposent ainsi :
// SENSOR_LORA_ID#param1:value1,param2,value2,etc...
// 
// SENSOR_LORA_ID : grâce à lui, on identifiera le type de capteur
// paramX         : paramètre X du capteur
// valueX         : Valeur X du capteur
//
// Exemple :
// ENV#temp:27.82,pres:963.3,humi:32.8,gasr:21079
// 
// où ENV est le capteur environementale, "temp" désigne la températeur,
// et 27.82 la valeur associée, etc...

var LORA_PAYLOAD_SENSOR_DATA_SEPARATOR = "#";
var LORA_PAYLOAD_DATA_SEPARATOR = ",";
var LORA_PAYLOAD_DATA_VALUE_SEPARATOR = ":";

var SENSOR_LORA_ID_ENV = "ENV";
var SENSOR_LORA_ID_IFR = "IFR";
var SENSOR_LORA_ID_TAM = "TAM";
var SENSOR_LORA_ID_AIR = "AIR";


//----------------------------------------------------------------------------------------//
//--------------------                ENVIRONMENT SENSOR              --------------------//
//----------------------------------------------------------------------------------------//


var bme;

/** Return data:
  new             // is this a new measurement?
  temperature     // degrees C
  pressure        // hPa
  humidity        // % rH
  gas_resistance  // Ohms
*/
function getEnvironmentData(bme){
  var ret_string = SENSOR_LORA_ID_ENV + LORA_PAYLOAD_SENSOR_DATA_SEPARATOR;
  bme.perform_measurement();
  var environment_data = bme.get_sensor_data();
  ret_string += "temp" +  LORA_PAYLOAD_DATA_VALUE_SEPARATOR + environment_data.temperature.toFixed(2);
  ret_string += LORA_PAYLOAD_DATA_SEPARATOR + "pres" + LORA_PAYLOAD_DATA_VALUE_SEPARATOR + environment_data.pressure.toFixed(1);
  ret_string += LORA_PAYLOAD_DATA_SEPARATOR + "humi" + LORA_PAYLOAD_DATA_VALUE_SEPARATOR + environment_data.humidity.toFixed(1);
  ret_string += LORA_PAYLOAD_DATA_SEPARATOR + "gasr" + LORA_PAYLOAD_DATA_VALUE_SEPARATOR + Math.round(environment_data.gas_resistance);
  
  return ret_string;
}



//----------------------------------------------------------------------------------------//
//--------------------                 INFRA RED SENSOR               --------------------//
//----------------------------------------------------------------------------------------//


var SLAVE_ADDR = 0x64;

var ST1   = 0x4;
var INTCAUSE = 0x5; 
var IR    = 0x6; 
var TEMP  = 0x8;
var ST2   = 0xA; 
var INTEN = 0x13;
var CNTL1 = 0x14;
var CNTL2 = 0x15;
var CNTL3 = 0x16;


//fonction fournie par l'assistant qui permet de faire le complément à 2
//en cas de présence de l'activation du bit de signe 
function two_complement(value_H, value_L) {
  if(value_H >= 256 || value_L >= 256) {
    return NaN;
  }

  if(value_H >= 128) {// negative value
    value_H = value_H - 128;
    return (value_H*256 + value_L) - 32768;
  } else { // positive value
    return value_H*256 + value_L;
  }
}

function wait_m(millis)
{
    var date = new Date();
    var curDate = null;
    do { curDate = new Date(); }
    while(curDate-date < millis);
}

//lecture
function i2c_read(i2c, address, burst){
  i2c.writeTo(SLAVE_ADDR, address);
  var ret = i2c.readFrom(SLAVE_ADDR, burst);
  return ret;
}

//écriture
function i2c_write(i2c, address, value){
  i2c.writeTo(SLAVE_ADDR, address, value);
}


function init_ir(i2c){
  wait_m(10);
  var deviceID = i2c_read(i2c,0,1);

  //soft reset 
  i2c_write(i2c, CNTL3, 0xFF);
  //interrupt source setting : Data ready
  i2c_write(i2c, INTEN, 0xE1);
  //FC setting : Cut-off frequency 0.45 hz for both IR and TEMP sensor 
  i2c_write(i2c, CNTL1, 0xEF);
  //set Continuous mode
  i2c_write(i2c, CNTL2, 0xFD);

  print("ok : ", parseInt(deviceID));
}


/*
  From datasheet
  10.5.1. Normal Read-out Procedure
  (1) Read out ST1 register
  DRDY: DRDY bit shows whether the state is "Data Ready" or not.
  DRDY = "0" means "No Data Ready".
  DRDY = "1" means "Data Ready".
  It is recommended that measurement data is read out when DRDY = "1".
  (2) Read out INTCAUSE register
  Interrupt factors can be found out by reading out INTCAUSE register.
  Refer to 13. Registers Functional Descriptions for details.
  When starting reading-out of INTCAUSE register, measurement data are transferred to read-out
  registers and retained. (Data protection)
  INTN returns to "Hi-Z" after reading out INTCAUSE register.
  (3) Read out measurement data
  (4) Read out ST2 resister (Required Operation)
*/
function getInfraRedData(){
  
  var ret_string = SENSOR_LORA_ID_IFR + LORA_PAYLOAD_SENSOR_DATA_SEPARATOR;
  
  //wait for interrupt (active low) 
  while(E10.read());
  //(1)
  var data_ready = i2c_read(i2c, ST1, 1);
  if(parseInt(data_ready) == 0xFF){
    //(2)
    var intcause = i2c_read(i2c, INTCAUSE, 1);
    if(parseInt(intcause) == 0xE1){
      //(3)
      var temp_ar = i2c_read(i2c, TEMP, 2);
      
      var temp = 0.0019837 * two_complement(temp_ar[1], temp_ar[0]) + 25; 
      
      ret_string += "temp" +  LORA_PAYLOAD_DATA_VALUE_SEPARATOR + temp.toFixed(2);

      var ir_val = i2c_read(i2c, IR, 2);

      var ir = 0.4578 * two_complement(ir_val[1], ir_val[0]);
      
      ret_string += LORA_PAYLOAD_DATA_SEPARATOR + "ir" +  LORA_PAYLOAD_DATA_VALUE_SEPARATOR + ir.toFixed(4);

      //(4)
      var data_overrun = i2c_read(i2c, ST2, 1);
      
      
    }
  }
  return ret_string;
}

/*setInterval(function() {
  getInfraRedData();
}, 2000);*/


//----------------------------------------------------------------------------------------//
//--------------------                AIR QUALITY SENSOR              --------------------//
//----------------------------------------------------------------------------------------//


// TODO


//----------------------------------------------------------------------------------------//
//--------------------                   TAMPER SENSOR                --------------------//
//----------------------------------------------------------------------------------------//


var tamper_sensor = false;

var TAMPER_PIN_INT = E10;

var TAMPER_ACTIVE_PAYLOAD = SENSOR_LORA_ID_TAM + LORA_PAYLOAD_SENSOR_DATA_SEPARATOR + "ACTIVE";




//========================================================================================//
//====================                RN2483 MAC COMMAND              ====================//
//========================================================================================//

// Mac get and set commands

var CMD_DEVADDR    = "devaddr";
var CMD_DEVEUI     = "deveui";
var CMD_APPEUI     = "appeui";
var CMD_PWRIDX     = "pwridx";
var CMD_DR         = "dr";
var CMD_ADR        = "adr";
var CMD_RETX       = "retx";
var CMD_RXDELAY1   = "rxdelay1";
var CMD_AR         = "ar";
var CMD_RX2        = "rx2";
var CMD_CH         = "ch";
var CMD_CH_FREQ    = "freq";
var CMD_CH_DCYCLE  = "dcycle";
var CMD_CH_DRRANGE = "drrange";
var CMD_CH_STATUS  = "status";

// set specific
var CMD_NWKSKEY  = "nwkskey";
var CMD_APPSKEY  = "appskey";
var CMD_APPKEY   = "appkey";
var CMD_BAT      = "bat";
var CMD_LINKCHK  = "linkchk";


// get specific
var CMD_BAND     = "band";
var CMD_RXDELAY2 = "rxdelay2";
var CMD_DCYCLEPS = "dcycleps";
var CMD_MRGN     = "mrgn";
var CMD_GWNB     = "gwnb";
var CMD_STATUS   = "status";


// TX type
var TX_CNF   = "cnf";
var TX_UNCNF = "uncnf";


// Join Mode
var MODE_OTAA     = "otaa";
var MODE_ABP      = "abp";



// Mac Response
var RN2483_MAC_RESP_OK                = "ok";
var RN2483_MAC_RESP_INVALID_PARAM     = "invalid_param";
var RN2483_MAC_RESP_DENIED            = "denied";
var RN2483_MAC_RESP_ACCEPTED          = "accepted";
var RN2483_MAC_RESP_MAC_TX_OK         = "mac_tx_ok";
var RN2483_MAC_RESP_MAC_RX            = "mac_rx";
var RN2483_MAC_RESP_NO_FREE_CHANNEL   = "no_free_ch";
var RN2483_MAC_RESP_BUSY              = "busy";



/**
 * Configure le module avec ses valeurs par défauts, et une fréquence de
 * communication demandée
 * serialPort   : Le port serial du module
 * band         : fréquence de communication
 */
function RN2483macReset(serial,band){
  var serialString = "mac reset " + band;
  serial.println(serialString);
}



/**
 * demande au module d'envoyer un payload au réseau
 * serialPort   : Le port serial du module
 * type         : type du payload (cnf: demande confirmation du reçu, uncnf: par de confirmation)
 * portno       : Numéro du port de destination
 * data         : le Payload à envoyer
 */
function RN2483macTx(serial,type,portno,data){
  var serialString = "mac tx " + type + " " + portno + " " + data;
  serial.println(serialString);
}





/**
 * demande au module de rejoindre le réseau configurer
 * serialPort   : Le port serial du module
 * mode         : mode procédure pour rejoindre le réseau
 */
function RN2483macJoin(serial, mode){
  var serialString = "mac join " + mode;
  serial.println(serialString);
}


/**
 * Sauvegarde les configurations actuels du module sur son EPROM
 * serialPort   : Le port serial du module
 */
function RN2483macSave(serial){
  var serialString = "mac save";
  serial.println(serialString);
}



function RN2483macForceEnable(serial){
  var serialString = "mac forceENABLE";
  serial.println(serialString);
}



/**
 * Interronot le module LoRa
 * serialPort   : Le port serial du module
 */
function RN2483macPause(serial){
  var serialString = "mac pause";
  serial.println(serialString);
}



/**
 * Relance le module LoRa
 * serialPort   : Le port serial du module
 */
function RN2483macResume(serial){
  var serialString = "mac resume";
  serial.println(serialString);
}



/**
 * Fonction pour modifier une configuration du module Lora
 * serialPort   : Le port serial du module
 * command      : la commande (devEUI/appEUI)
 * param1       : Certaine commande demande un paramètre
 * param1       : Certaine commande demande un deuxième paramètre
 * param1       : Certaine commande demande un troisième paramètre
 */
function RN2483macSet(serial, command, param1, param2, param3){
  var serialString = "mac set ";
  
  switch(command){
    case CMD_DEVADDR:
    case CMD_DEVEUI:
    case CMD_APPEUI:
    case CMD_NWKSKEY:
    case CMD_APPSKEY:
    case CMD_APPKEY:
    case CMD_PWRIDX:
    case CMD_DR:
    case CMD_ADR:
    case CMD_BAT:
    case CMD_RETX:
    case CMD_LINKCHK:
    case CMD_RXDELAY1:
    case CMD_AR:
      // param1 is the value to set
      serialString += command + " " + param1;
      break;
    case CMD_RX2:
      // param1 is the data rate and param2 is the frequency
      serialString += command + " " + param1 + " " + param2;
      break;
    case CMD_CH:
      // param1 is the parameter, param2 is the channel id, param3 is the value to set
      serialString += command + " " + param1 + " " + param2  + " " + param3;
      break;
    default:
      console.log("Unkown mac set command");
      return;
  }
  
  Serial3.println(serialString);
}


/**
 * Fonction pour obtenir une configuration du module Lora
 * serialPort   : Le port serial du module
 * command      : la commande (devEUI/appEUI)
 * param1       : Certaine commande demande un paramètre
 * param1       : Certaine commande demande un deuxième paramètre
 */
function RN2483macGet(serialPort, command, param1,param2){
  var serialString = "mac get ";
  
  switch(command){
    case CMD_DEVADDR:
    case CMD_DEVEUI:
    case CMD_APPEUI:
    case CMD_PWRIDX:
    case CMD_DR:
    case CMD_BAND:
    case CMD_ADR:
    case CMD_RETX:
    case CMD_RXDELAY1:
    case CMD_RXDELAY2:
    case CMD_DCYCLEPS:
    case CMD_MRGN:
    case CMD_GWNB:
    case CMD_STATUS:
      serialString += command;
      break;
    case CMD_RX2:
      // param1 is band frequency
      serialString += command + " " + param1;
      break;
    case CMD_CH:
      // param1 is the channel parameter, and param2 is the channel id.
      serialString += command + " " + param1 + " " + param2;
      break;
    default:
      console.log("Unkown mac get command");
      return;
  }
  
  Serial3.println(serialString);
}




save();
