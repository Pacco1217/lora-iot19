// Mac get and set commands

var CMD_DEVADDR    = "devaddr";
var CMD_DEVEUI     = "deveui";
var CMD_APPEUI     = "appeui";
var CMD_PWRIDX     = "pwridx";
var CMD_DR         = "dr";
var CMD_ADR        = "adr";
var CMD_RETX       = "retx";
var CMD_RXDELAY1   = "rxdelay1";
var CMD_AR         = "ar";
var CMD_RX2        = "rx2";
var CMD_CH         = "ch";
var CMD_CH_FREQ    = "freq";
var CMD_CH_DCYCLE  = "dcycle";
var CMD_CH_DRRANGE = "drrange";
var CMD_CH_STATUS  = "status";

// set specific
var CMD_NWKSKEY  = "nwkskey";
var CMD_APPSKEY  = "appskey";
var CMD_APPKEY   = "appkey";
var CMD_BAT      = "bat";
var CMD_LINKCHK  = "linkchk";


// get specific
var CMD_BAND     = "band";
var CMD_RXDELAY2 = "rxdelay2";
var CMD_DCYCLEPS = "dcycleps";
var CMD_MRGN     = "mrgn";
var CMD_GWNB     = "gwnb";
var CMD_STATUS   = "status";


// TX type
var TX_CNF   = "cnf";
var TX_UNCNF = "uncnf";


// Join Mode
var MODE_OTAA     = "otaa";
var MODE_ABP      = "abp";



// Mac Response

var RN2483_MAC_RESP_OK                = "ok";
var RN2483_MAC_RESP_INVALID_PARAM     = "invalid_param";



//RN2483macReset(Serial3,"868")

function RN2483macReset(serial,band){
  var serialString = "mac reset " + band;
  serial.println(serialString);
}



//RN2483macTx(Serial3,TX_CNF,1,"AB")

function RN2483macTx(serial,type,portno,data){
  var serialString = "mac tx " + type + " " + portno + " " + data;
  serial.println(serialString);
}





//RN2483macJoin(Serial3,MODE_OTAA)

function RN2483macJoin(serial, mode){
  var serialString = "mac join " + mode;
  serial.println(serialString);
}


//RN2483macSave(Serial3)

function RN2483macSave(serial){
  var serialString = "mac save";
  serial.println(serialString);
}


//RN2483macForceEnable(Serial3)

function RN2483macForceEnable(serial){
  var serialString = "mac forceENABLE";
  serial.println(serialString);
}



//RN2483macPause(Serial3)

function RN2483macPause(serial){
  var serialString = "mac pause";
  serial.println(serialString);
}



//RN2483macResume(Serial3)

function RN2483macResume(serial){
  var serialString = "mac resume";
  serial.println(serialString);
}


//RN2483macSet(Serial3,CMD_APPEUI,"70B3D57ED0018DAC")
//RN2483macSet(Serial3,CMD_APPKEY,"3AB62F727732E9B6BB60F0572D1E846B")

function RN2483macSet(serial, command, param1, param2, param3){
  var serialString = "mac set ";
  
  switch(command){
    case CMD_DEVADDR:
    case CMD_DEVEUI:
    case CMD_APPEUI:
    case CMD_NWKSKEY:
    case CMD_APPSKEY:
    case CMD_APPKEY:
    case CMD_PWRIDX:
    case CMD_DR:
    case CMD_ADR:
    case CMD_BAT:
    case CMD_RETX:
    case CMD_LINKCHK:
    case CMD_RXDELAY1:
    case CMD_AR:
      // param1 is the value to set
      serialString += command + " " + param1;
      break;
    case CMD_RX2:
      // param1 is the data rate and param2 is the frequency
      serialString += command + " " + param1 + " " + param2;
      break;
    case CMD_CH:
      // param1 is the parameter, param2 is the channel id, param3 is the value to set
      serialString += command + " " + param1 + " " + param2  + " " + param3;
      break;
    default:
      console.log("Unkown mac set command");
      return;
  }
  
  serial.println(serialString);
}


//RN2483macGet(Serial3,CMD_APPEUI)

function RN2483macGet(serial, command, param1,param2){
  var serialString = "mac get ";
  
  switch(command){
    case CMD_DEVADDR:
    case CMD_DEVEUI:
    case CMD_APPEUI:
    case CMD_PWRIDX:
    case CMD_DR:
    case CMD_BAND:
    case CMD_ADR:
    case CMD_RETX:
    case CMD_RXDELAY1:
    case CMD_RXDELAY2:
    case CMD_DCYCLEPS:
    case CMD_MRGN:
    case CMD_GWNB:
    case CMD_STATUS:
      serialString += command;
      break;
    case CMD_RX2:
      // param1 is band frequency
      serialString += command + " " + param1;
      break;
    case CMD_CH:
      // param1 is the channel parameter, and param2 is the channel id.
      serialString += command + " " + param1 + " " + param2;
      break;
    default:
      console.log("Unkown mac get command");
      return;
  }
  
  serial.println(serialString);
}


// mac set adr on
// mac set dr 5
// mac save

save();
