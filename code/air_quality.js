var SLAVE_ADDR = 0x58;
var CRC8_POLYNOMIAL = 0x31; 
  

var i2c = new I2C();
i2c.setup({sda:C9,scl:A8});

//fonction fournie par l'assistant qui permet de faire le complément à 2
//en cas de présence de l'activation du bit de signe 
function two_complement(value_H, value_L) {
  if(value_H >= 256 || value_L >= 256) {
    return NaN;
  }

  if(value_H >= 128) {// negative value
    value_H = value_H - 128;
    return (value_H*256 + value_L) - 32768;
  } else { // positive value
    return value_H*256 + value_L;
  }
}

function wait_m(millis)
{
    var date = new Date();
    var curDate = null;
    do { curDate = new Date(); }
    while(curDate-date < millis);
}

//lecture
function i2c_read(address, burst, wait){
  for (i=0; i < address.length; i++){
    i2c.writeTo(SLAVE_ADDR, address);
  }
  wait_m(wait);
  var ret = i2c.readFrom(SLAVE_ADDR, burst);
  return ret;
}

//écriture
function i2c_write(address, value){
  i2c.writeTo(SLAVE_ADDR, address, value);
}

function is_crc_ok(data, checksum){
  var crc = 0xff; //init value

  for(cur_byte = 0; cur_byte < data.length; cur_byte++){
    crc = (crc ^ (data[cur_byte])) & 0xff;
    for (crc_bit = 8; crc_bit > 0; --crc_bit) {
              if (crc & 0x80)
                  crc = ((crc << 1) ^ CRC8_POLYNOMIAL) & 0xff;
              else
                  crc = (crc << 1) & 0xff;
    }
  }

  //print("crc val : " +  crc);// for debug

  if(crc == checksum)
    return true;
  else
    return false;
}

function i2c_reset(){
  i2c.writeTo(0x00, 0x06);
}

function init(){
  wait_m(10);
  i2c_reset();

  /* test : ok
  var get_id = i2c_read([0x36, 0x82], 9, 0);
  var test = i2c_read([0x20, 0x32], 3, 300);
  print("ok : ", test);
  print("crc is ok ? : " + is_crc_ok([test[0], test[1]], test[2]));
  */

  //init_air_quality
  i2c_read([0x20, 0x03], 1, 20);
  wait_m(20);
}

init();

setInterval(function() {
  var get_measure = i2c_read([0x20, 0x08], 6, 15);

  //print("get : ", get_measure);
  //print("crc is ok ? : " + is_crc_ok([get_measure[0], get_measure[1]], get_measure[2]));
  //print("crc is ok ? : " + is_crc_ok([get_measure[3], get_measure[4]], get_measure[5]));

  if(is_crc_ok([get_measure[0], get_measure[1]], get_measure[2]) && 
     is_crc_ok([get_measure[3], get_measure[4]], get_measure[5])){

     var CO2 = get_measure[0] * 256 + get_measure[1];
     var TVOC = get_measure[3] * 256 + get_measure[4];
     print("CO2 : " + CO2 + " ppm"); 
     print("TVOC : " + TVOC + " ppb");
  }
}, 1000);
