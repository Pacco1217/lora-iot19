var pitches = {
  'a':220.00,
  'b':246.94,
  'c':261.63,
  'd':293.66,
  'e':329.63,
  'f':349.23,
  'g':392.00,
  'A':440.00,
  'B':493.88,
  'C':523.25,
  'D':587.33,
  'E':659.26,
  'F':698.46,
  'G':783.99
};


var BUZZ = E9; 

function wait_m(millis)
{
    var date = new Date();
    var curDate = null;
    do { curDate = new Date(); }
    while(curDate-date < millis);
}

function freq(f) { 
  if (f===0) digitalWrite(BUZZ,0);
  else analogWrite(BUZZ, 0.5, { freq: f } );
}


function step() {
  var ch = tune[pos];
  if (ch !== undefined) pos++;
  if (ch in pitches) freq(pitches[ch]);
  else freq(0); // off
  //wait_m(200);
}

var tune = "E         D     E     D  E b D C a ce a b e g b  C e E DE  D E b D C a ce   a    be       C b a E D E DE b    DC a c e a be g b  C e ED E  DE b D Ca c e     a b e C b ab   CD E g F E D f E    DC e D C b e E e  ED E D E DE D E  DE D E bD  C a c e a b e g b  Ce E D E DE b D C a c ea b e C b a"; 
pos=0;

setInterval(step, 100);