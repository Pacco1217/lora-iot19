
var i2c = new I2C();
i2c.setup({sda:C9,scl:A8});
var bme = require("BME680").connectI2C(i2c, {addr:0x77});
var environment_data;

setInterval(function() {

/** Return data:
  new             // is this a new measurement?
  temperature     // degrees C
  pressure        // hPa
  humidity        // % rH
  gas_resistance  // Ohms
*/
  environment_data = bme.get_sensor_data();
  console.log("Envs infos : ", JSON.stringify(environment_data,null,2));
  bme.perform_measurement();
}, 1000);


save();