var SLAVE_ADDR = 0x64;

var ST1   = 0x4;
var INTCAUSE = 0x5; 
var IR    = 0x6; 
var TEMP  = 0x8;
var ST2   = 0xA; 
var INTEN = 0x13;
var CNTL1 = 0x14;
var CNTL2 = 0x15;
var CNTL3 = 0x16;

var i2c = new I2C();
i2c.setup({sda:C9,scl:A8});

//fonction fournie par l'assistant qui permet de faire le complément à 2
//en cas de présence de l'activation du bit de signe 
function two_complement(value_H, value_L) {
  if(value_H >= 256 || value_L >= 256) {
    return NaN;
  }

  if(value_H >= 128) {// negative value
    value_H = value_H - 128;
    return (value_H*256 + value_L) - 32768;
  } else { // positive value
    return value_H*256 + value_L;
  }
}

function wait_m(millis)
{
    var date = new Date();
    var curDate = null;
    do { curDate = new Date(); }
    while(curDate-date < millis);
}

//lecture
function i2c_read(address, burst){
  i2c.writeTo(SLAVE_ADDR, address);
  var ret = i2c.readFrom(SLAVE_ADDR, burst);
  return ret;
}

//écriture
function i2c_write(address, value){
  i2c.writeTo(SLAVE_ADDR, address, value);
}


function init_ir(){
  wait_m(10);
  var deviceID = i2c_read(0,1);

  //soft reset 
  i2c_write(CNTL3, 0xFF);
  //interrupt source setting : Data ready
  i2c_write(INTEN, 0xE1);
  //FC setting : Cut-off frequency 0.45 hz for both IR and TEMP sensor 
  i2c_write(CNTL1, 0xEF);
  //set Continuous mode
  i2c_write(CNTL2, 0xFD);

  print("ok : ", parseInt(deviceID));
}

init_ir();


/*
// test du complément à 2 
var input = 0x8550; //-31'408
var input_h = 0x85;
var input_l = 0x50;

print("two's complement 1 : " + two_complement(input_h, input_l));
*/

/*
  From datasheet
  10.5.1. Normal Read-out Procedure
  (1) Read out ST1 register
  DRDY: DRDY bit shows whether the state is "Data Ready" or not.
  DRDY = "0" means "No Data Ready".
  DRDY = "1" means "Data Ready".
  It is recommended that measurement data is read out when DRDY = "1".
  (2) Read out INTCAUSE register
  Interrupt factors can be found out by reading out INTCAUSE register.
  Refer to 13. Registers Functional Descriptions for details.
  When starting reading-out of INTCAUSE register, measurement data are transferred to read-out
  registers and retained. (Data protection)
  INTN returns to "Hi-Z" after reading out INTCAUSE register.
  (3) Read out measurement data
  (4) Read out ST2 resister (Required Operation)
*/
setInterval(function() {
    //wait for interrupt (active low) 
    while(E10.read());
    //(1)
    var data_ready = i2c_read(ST1, 1);
    if(parseInt(data_ready) == 0xFF){
      //(2)
      var intcause = i2c_read(INTCAUSE, 1);
      if(parseInt(intcause) == 0xE1){
        //(3)
        var temp_ar = i2c_read(TEMP, 2);
        /*
        var temp_hi = parseInt(temp_ar[1]) * 256;
        var temp_low = parseInt(temp_ar[0]);
        if((temp_hi & 0x8000) > 0){
          temp_hi = ~temp_hi + 1;
        }
        */
        var temp = 0.0019837 * two_complement(temp_ar[1], temp_ar[0]) + 25; 
        print("temperature : "+ temp + " °c");

        var ir_val = i2c_read(IR, 2);

        /* 
        //non fonctionnel
        var ir_hi = parseInt(ir_val[1]) * 256;
        var ir_low = parseInt(ir_val[0]);
        if((ir_hi & 0x8000) > 0){
          ir_hi = ~ir_hi + 1;
        }
        */
        var ir = 0.4578 * two_complement(ir_val[1], ir_val[0]);
        print("IR : " + ir + " pA");

        //(4)
        var data_overrun = i2c_read(ST2, 1);
      }
    }
}, 1000); 